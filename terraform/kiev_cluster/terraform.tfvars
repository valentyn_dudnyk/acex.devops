aws_profile = "default"
aws_region = "eu-west-3"
vpc_cidr = "192.168.0.0/16"
allowed_cidr_ranges = ["46.211.122.54/32"]
cidrs = {
  public1 = "192.168.11.0/24"
  public2 = "192.168.22.0/24"
  public3 = "192.168.222.0/24"
  private1 = "192.168.33.0/24"
  private2 = "192.168.44.0/24"
  private3 = "192.168.43.0/24"
  rds1 = "192.168.55.0/24"
  rds2 = "192.168.66.0/24"
  rds3 = "192.168.77.0/24"
}
#ip from 2ip.ru
#accessip = "194.105.145.87/32"
accessip = "0.0.0.0/0"
domain_name = "rogachov"
db_instance_class = "db.t2.micro"
dbname = "rogachovdb"
dbuser = "rogachovuser"
dbpassword = "rogachovdb"
elb_healthy_threshold = "2"
elb_unhealthy_threashold = "2"
elb_timeout = "3"
elb_interval = "30"

dev_instance_type = "t2.medium"
dev_ami = "ami-0acfe51b617393c70"
public_key_path = "/root/.ssh/id_rsa.pub"
key_name = "jenkinskey"

asg_max = "2"
asg_min = "1"
asg_grace = "300"
asg_hct = "EC2"
asg_cap = "2"
lc_instance_type = "t2.micro"

delegation_set = "N1HD42OPWFOUVY"
ENV_TAG = "TESTING"
vpc_name = "QA"
eks_iam_users = [
  {
    user_arn = "arn:aws:iam::878033925423:user/api-acex"
    username = "api-acex"
    group    = "system:masters"
  },
  {
    user_arn = "arn:aws:iam::878033925423:user/acex-infra-test"
    username = "acex-infra-test"
    group    = "system:masters"
  },
]
eks_iam_users_count = 2
dbpassword = "kZhKXwuZkyjCkq9g"