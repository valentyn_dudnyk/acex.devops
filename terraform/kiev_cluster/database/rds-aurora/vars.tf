variable "name" {
}

variable "vpc_id" {
}

variable "subnets" {
  description = "[] List of database subnets"
  type = "list"
}

variable "trusted_cidr_blocks" {
  description = "[] List of trusted cidr blocks"
  type = "list"
}

variable "env" {
}

variable "dbpassword" {
  
}

variable "dns_zone_id" {

}
