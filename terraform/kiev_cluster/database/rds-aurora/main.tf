module "aurora" {
  source                          = "terraform-aws-modules/rds-aurora/aws"
  version                         = "1.14.0"
  name                            = "${var.name}"
  engine                          = "aurora-mysql"
  engine_version                  = "5.7.12"
  subnets                         = ["${var.subnets}"]
  vpc_id                          = "${var.vpc_id}"
  replica_count                   = 1
  instance_type                   = "db.r4.large"
  apply_immediately               = true
  publicly_accessible             = true
  skip_final_snapshot             = true
  db_parameter_group_name         = "${aws_db_parameter_group.aurora_db_mysql57_parameter_group.id}"
  db_cluster_parameter_group_name = "${aws_rds_cluster_parameter_group.aurora_cluster_mysql57_parameter_group.id}"
  enabled_cloudwatch_logs_exports = ["audit", "error", "general", "slowquery"]
  password                        = "${var.dbpassword}"
}

resource "aws_route53_record" "dns" {
  zone_id = "${var.dns_zone_id}"
  name    = "${var.name}"
  type    = "CNAME"
  ttl     = 5
  records = ["${module.aurora.this_rds_cluster_endpoint}"]
}

resource "aws_db_parameter_group" "aurora_db_mysql57_parameter_group" {
  name        = "${var.name}-aurora-db-mysql57-parameter-group"
  family      = "aurora-mysql5.7"
  description = "${var.name}-aurora-db-mysql57-parameter-group"
}

resource "aws_rds_cluster_parameter_group" "aurora_cluster_mysql57_parameter_group" {
  name        = "${var.name}-aurora-mysql57-cluster-parameter-group"
  family      = "aurora-mysql5.7"
  description = "${var.name}-aurora-mysql57-cluster-parameter-group"
}

resource "aws_security_group_rule" "allow_access" {
  type                     = "ingress"
  from_port                = "${module.aurora.this_rds_cluster_port}"
  to_port                  = "${module.aurora.this_rds_cluster_port}"
  protocol                 = "tcp"
  cidr_blocks              = ["${var.trusted_cidr_blocks}"]
  security_group_id        = "${module.aurora.this_security_group_id}"
}
