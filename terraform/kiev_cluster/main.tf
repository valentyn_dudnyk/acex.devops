provider "aws" {
  region  = "${var.aws_region}"
  profile = "${var.aws_profile}"
}
provider "aws" {
  alias  = "us-east"
  region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "acex-test-new"
    key = "terraform/terraforms.tfstate"
    region = "eu-west-3"
  }

}

#------ IAM -------
#S3 Access
resource "aws_iam_instance_profile" "s3_access_profile" {
  name = "s3_access"
  role = "${aws_iam_role.s3_access_role.name}"
}

resource "aws_iam_policy" "eks_route53" {
  name        = "eks_qa-route53_policy"
  path        = "/"
  description = "Route53 Policy for EKS"
  policy      = "${var.route53_role_policy_document}"
}

resource "aws_iam_role_policy" "s3_access_policy" {
  name = "s3_accesss_policy"
  role = "${aws_iam_role.s3_access_role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
     "Effect": "Allow",
     "Action": "s3:*",
     "Resource": "*"
   }
  ]
}
EOF
}

resource "aws_iam_role" "s3_access_role" {
  name = "s3_access_role"

  assume_role_policy = <<EOF
{
   "Version": "2012-10-17",
   "Statement": [
       {
         "Action": "sts:AssumeRole",
         "Principal": {
             "Service": "ec2.amazonaws.com"
         },
           "Effect": "Allow",
           "Sid": ""
        }
       ]
}
EOF
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "1.46.0"
  name = "${var.vpc_name}"
  cidr = "${var.vpc_cidr}"

  #azs             = ["eu-west-3", "eu-west-2", "eu-central-1"]
  azs             = ["eu-west-3c", "eu-west-3a", "eu-west-3b"]
  private_subnets = ["${var.cidrs["private1"]}", "${var.cidrs["private2"]}","${var.cidrs["private3"]}"]
 // database_subnets = ["${var.cidrs["rds1"]}", "${var.cidrs["rds2"]}", "${var.cidrs["rds3"]}"]
  public_subnets = ["${var.cidrs["public1"]}", "${var.cidrs["public2"]}","${var.cidrs["public3"]}"]

  private_subnet_tags {
    "kubernetes.io/cluster/eks_${var.ENV_TAG}" = "shared"
  }
  public_subnet_tags {
    "kubernetes.io/cluster/eks_${var.ENV_TAG}" = "shared"
  }

  enable_dns_hostnames = true
  enable_nat_gateway = true
  single_nat_gateway = true
  tags = {
    Environment = "${var.ENV_TAG}"
  }
}


module "eks" {
  source       = "terraform-aws-modules/eks/aws"
  cluster_name = "eks_${var.ENV_TAG}"
  subnets      = ["${module.vpc.private_subnets}","${module.vpc.public_subnets}"]
  vpc_id       = "${module.vpc.vpc_id}"
  version      = "=4.0.0"

  worker_groups = [
    {
      instance_type = "t3.large"
      asg_max_size  = 5
      asg_min_size  = 1
      asg_desired_capacity = 1
      autoscaling_enabled = true
      protect_from_scale_in = true
      subnets = "${join(",", module.vpc.private_subnets)}"
    }
  ]

  map_users = "${var.eks_iam_users}"
  map_users_count = "${var.eks_iam_users_count}"
  workers_additional_policies = ["${aws_iam_policy.eks_route53.arn}"]
  workers_additional_policies_count = "1"

  tags = {
    environment = "${var.ENV_TAG}"
  }
}


#
# EKS addons
#

data "aws_eks_cluster_auth" "eks" {
  name = "eks_${var.ENV_TAG}"
}

provider "kubernetes" {
  host                    = "${module.eks.cluster_endpoint}"
  cluster_ca_certificate  = "${base64decode(module.eks.cluster_certificate_authority_data)}"
  token                   = "${data.aws_eks_cluster_auth.eks.token}"
  load_config_file        = false
}

resource "kubernetes_service_account" "tiller" {
  metadata {
    name      = "tiller"
    namespace = "kube-system"
  }

  automount_service_account_token = true
}

resource "kubernetes_cluster_role_binding" "tiller" {
  metadata {
    name = "tiller-cluster-admin"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  subject {
    api_group = ""
    kind      = "ServiceAccount"
    name      = "tiller"
    namespace = "kube-system"
  }
}

provider "helm" {
  debug = true
  install_tiller = true
  service_account = "tiller"
  kubernetes {
    host                    = "${module.eks.cluster_endpoint}"
    cluster_ca_certificate  = "${base64decode(module.eks.cluster_certificate_authority_data)}"
    token                   = "${data.aws_eks_cluster_auth.eks.token}"
  }
}

resource "helm_release" "cluster_autoscaler" {
  name      = "ca"
  namespace = "kube-system"
  chart     = "stable/cluster-autoscaler"

  set {
    name  = "rbac.create"
    value = "true"
  }
  set {
    name = "sslCertPath"
    value = "/etc/ssl/certs/ca-bundle.crt"
  }
  set {
    name = "cloudProvider"
    value = "aws"
  }
  set {
    name = "awsRegion"
    value = "${var.aws_region}"
  }
  set {
    name = "autoDiscovery.clusterName"
    value = "eks_${var.ENV_TAG}"
  }
  set {
    name = "autoDiscovery.enabled"
    value = "true"
  }
}


#------- Registry

#resource "aws_ecr_repository" "cicd" {
#  name = "cicd"
#}


#---------------------------------------------------------

resource "aws_key_pair" "terra_auth" {
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDgOxXTJ/Jo+PoD+xKBni8LQep7hpxcVHnjVoC0ZzrHmuFTD0o9a16z0swsea70o0HFmI9GwIFkF8v3CxaWVxQC46sRKfRYsIr36OWO76xFeDJyb9H2TmjQU2ka9DpvYC7lGNzdJoDOg2bLVkLlNM5H0RwJUSocvBoMgqbtFwWTw7AERUn2DNdHsm8yzD9SsfBlDPMS9hkJh30+gw3jvnmJKOTOpZswBn2bIBsan1DmcTUoiMYr+aapptSdiCId0hRs3Qb2a0+kWJBVkT0FGC2cqO53rTXEpSuDwXeXS9xmb6cXuZdo1UWEWyg/En2izKhiTFYgYLN2gMggOzU5eCCz irogachov@irogachovs-MacBook-Pro.local"
  key_name   = "${var.key_name}"
}

resource "aws_security_group" "terra_dev_sg" {
  name        = "terra_dev_sg"
  description = "For access to dev instance"
  vpc_id      = "${module.vpc.vpc_id}"

  #SSH
  ingress {
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = ["${var.accessip}"]
  }

  #HTTP
  ingress {
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
    cidr_blocks = ["${var.accessip}"]
  }

  #HTTP
  ingress {
    from_port   = 8080
    protocol    = "tcp"
    to_port     = 8080
    cidr_blocks = ["${var.accessip}"]
  }

  #HTTP
  ingress {
    from_port   = 50000
    protocol    = "tcp"
    to_port     = 50000
    cidr_blocks = ["${var.accessip}"]
  }

  #HTTP
  ingress {
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
    cidr_blocks = ["${var.accessip}"]
  }



  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}


data "aws_subnet_ids" "example" {
  vpc_id = "${module.vpc.vpc_id}"
}



resource "aws_instance" "terra_dev" {
  ami           = "${var.dev_ami}"
  instance_type = "${var.dev_instance_type}"
  disable_api_termination = true

  tags {
    Name = "Jenkins"
  }

  key_name               = "${aws_key_pair.terra_auth.id}"
  vpc_security_group_ids = ["${aws_security_group.terra_dev_sg.id}"]
 # iam_instance_profile   = "${aws_iam_instance_profile.s3_access_profile.id}"
  subnet_id              = "${element(module.vpc.public_subnets, 0)}"
}



# Internal DNS zone for easier access and naming of internal resources
resource "aws_route53_zone" "private" {
  name = "internal.acex.tech."

  vpc {
    vpc_id = "${module.vpc.vpc_id}"
  }

  # As per notes from https://www.terraform.io/docs/providers/aws/r/route53_zone.html
  #  Terraform provides both exclusive VPC associations defined in-line in this resource via
  #  vpc configuration blocks and a separate Zone VPC Association resource. At this time, you cannot
  #  use in-line VPC associations in conjunction with any aws_route53_zone_association resources
  #  with the same zone ID otherwise it will cause a perpetual difference in plan output. You can
  #  optionally use the generic Terraform resource lifecycle configuration block with ignore_changes
  #  to manage additional associations via the aws_route53_zone_association resource.
  lifecycle {
    ignore_changes = ["vpc"]
  }
}


#
# RDS
# Databases
#


module "db_customer_data" {
  source = "./database/rds-aurora"
  name = "customer-data"
  vpc_id = "${module.vpc.vpc_id}"
  subnets = ["${module.vpc.public_subnets}"]
  trusted_cidr_blocks = ["${module.vpc.private_subnets_cidr_blocks}","${var.allowed_cidr_ranges}"]
  dbpassword = "${var.dbpassword}"
  dns_zone_id = "${aws_route53_zone.private.zone_id}"
  env = "${var.ENV_TAG}"
}
