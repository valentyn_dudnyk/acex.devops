// Deploy_Backend_v2
// http://172.30.0.177:8080/job/QA/job/Deploy_Backend_v2/
// Repo URL: https://bitbucket.org/Femitelemedicine/devops.git
// Script Path: jenkins_pipelines/Deploy_Backend_v2.groovy

import org.yaml.snakeyaml.Yaml

@NonCPS
def yamlParse(def yamlText) {
  new org.yaml.snakeyaml.Yaml().load(yamlText)
}

//parameters {
//  choices(name: 'namespace',
//          choices: 'dev\nqa',
//          description: 'What door do you choose?')
//  text(defaultValue: '''journeysearch:
//          image: latest
//          service2:
//                  image: latest
//          service3:
//                  image: latest''',
//          name: 'services' )
//}

// currentBuild.description = "Deployment: ${namespace}"

node("buildnet")
        {

          def appRepo = '${GitUrl}'
          def bitbucketCredsId = 'bitbutket-key'
          def ecrRepo = '878033925423.dkr.ecr.eu-west-3.amazonaws.com'
          def ecrCredsId = 'ecr:eu-west-3:acex_test'

          cleanWs()
          try {


            def workspace = pwd()
            stage('Git checkout') {
              checkout([$class: 'GitSCM',
                        branches: [[name: '${GitBranch}']],
                        doGenerateSubmoduleConfigurations: false,
                        extensions: [
                                [$class: 'CleanCheckout']
                        ],
                        submoduleCfg: [],
                        userRemoteConfigs: [[credentialsId: bitbucketCredsId, url: appRepo]]
              ])
              shortCommit = sh(script: "git rev-parse --short HEAD", returnStdout: true).trim()
              repoName = sh(script: "git remote show origin -n |grep Fetch | sed  's/^[^/]*\\//\\//' | cut -c 2- |sed 's/.\\{4\\}\$//'", returnStdout: true).trim()

              stage('test') {
                  container('buildnet') {
                      println 'Code has been cloned current brach is: '
                      sh("pwd && ls -la")
                   //   sh("tail -f /dev/null")
                      istestpresent = sh(script: "find . -name *.Unit.Tests.csproj", returnStdout: true).trim()
                      println "variable is : ${istestpresent}"
                      def var = "${istestpresent}"
                      if (var?.trim()) {
                          sh("find . -name *.Unit.Tests.csproj -exec dotnet add {} package coverlet.msbuild \\;  -exec dotnet test {} /p:CollectCoverage=true /p:CoverletOutputFormat=opencover \\;")
                          sh("dotnet build-server shutdown")
                          // sh("tail -f /dev/null")
                          filexml = sh(script: "find . -name coverage.opencover.xml", returnStdout: true).trim()
                          sh("find . -name coverage.opencover.xml")
                          sh("dotnet sonarscanner begin /k:'${repoName}' /d:sonar.host.url='https://sonar.develop.acex.tech' /d:sonar.login='e14be968358bbb5e7f3d10af0a12edb5d65ab953' /d:sonar.cs.opencover.reportsPaths='${filexml}'  /d:sonar.coverage.exclusions='**Test*.cs'; dotnet build src/*.sln; dotnet sonarscanner end /d:sonar.login='e14be968358bbb5e7f3d10af0a12edb5d65ab953' ")
                      } else {
                              sh("dotnet sonarscanner begin /k:'${repoName}' /d:sonar.host.url='https://sonar.develop.acex.tech' /d:sonar.login='e14be968358bbb5e7f3d10af0a12edb5d65ab953'; dotnet build src/*.sln; dotnet sonarscanner end /d:sonar.login='e14be968358bbb5e7f3d10af0a12edb5d65ab953'")
                      }
                  //    sh("dotnet sonarscanner begin /k:'${repoName}' /d:sonar.host.url='https://sonar.develop.acex.tech' /d:sonar.login='e14be968358bbb5e7f3d10af0a12edb5d65ab953'; dotnet build src/*.sln; dotnet sonarscanner end /d:sonar.login='e14be968358bbb5e7f3d10af0a12edb5d65ab953'")
                  }
              }
            }

            stage('Build') {
              configFileProvider([configFile(fileId: 'kube-config', variable: 'kube_config')]) {
                sh("mkdir -p ~/.kube && cp ${kube_config} ~/.kube/config")
              }
              configFileProvider([configFile(fileId: 'aws', variable: 'aws')]) {
                sh("mkdir -p ~/.aws && cp ${aws} ~/.aws/credentials")
              }
              configFileProvider([configFile(fileId: 'dockerconf', variable: 'dockerconf')]) {
                sh("mkdir -p ~/.kube && cp ${dockerconf} docker-compose.yml")
              }
              sh("export KUBECONFIG=~/.kube/config")
              container('buildnet') {
                sh("echo Git Url has to be ${GitUrl}, git branch is ${GitBranch}, commit sha ${GitCommit} and ShortCommit ${shortCommit}")
                sh("sed -i 's/NAME/${ecrRepo}\\/dev\\/${repoName}/g' ./docker-compose.yml")
                sh("sed -i 's/TAG/${shortCommit}/g' ./docker-compose.yml")
                sh("ls -la")
                sh("cat ./docker-compose.yml")
                //  sh("tail -f /dev/null")
                sh("docker-compose build")
                sh("mkdir ./deploy && touch ./deploy/Dockerfile")
                sh("echo FROM ${ecrRepo}/dev/${repoName}:${shortCommit} > ./deploy/Dockerfile")
                docker.withRegistry("https://${ecrRepo}", ecrCredsId) {
                  dir('./deploy/') {
                    def customImage = docker.build("${ecrRepo}/dev/${repoName}:${shortCommit}")
                    customImage.push()
                    def customImageLatest = docker.build("${ecrRepo}/dev/${repoName}:latest")
                    customImageLatest.push()
                  }
                }
              }

            }



//    sendReportToSlack(namespace, params.services)
            currentBuild.result = "SUCCESS"

          } catch(e) {
//    sendReportToSlack(namespace, params.services, e)
            currentBuild.result = "FAILED"
            println "error: " + e
            throw e
          }
        }

node("helm")
{

    container('helm') {

        configFileProvider([configFile(fileId: 'kube-config', variable: 'kube_config')]) {
            sh("mkdir -p ~/.kube && cp ${kube_config} ~/.kube/config")
        }
        configFileProvider([configFile(fileId: 'aws', variable: 'aws')]) {
            sh("mkdir -p ~/.aws && cp ${aws} ~/.aws/credentials")
        }
        sh("export KUBECONFIG=~/.kube/config")
        stage('AutoDeploy to Dev space') {
            println 'Code has been cloned current brach is: '
            appName = sh(script: """kubectl get deployment dev-${repoName} -n dev   -o yaml |grep  'name: dev-acex-' |sed s/' '//g | sort  -u | cut -d: -f2""", returnStdout: true).trim()
            sh("kubectl get deployment -n dev")
            sh("kubectl  -n dev set image deployment dev-${repoName} ${appName}=878033925423.dkr.ecr.eu-west-3.amazonaws.com/dev/${repoName}:${shortCommit}")

        }
    }

}
