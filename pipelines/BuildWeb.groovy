// https://web-devs.test.acex.tech/index.html

import org.yaml.snakeyaml.Yaml

@NonCPS
def yamlParse(def yamlText) {
  new org.yaml.snakeyaml.Yaml().load(yamlText)
}

//parameters {
//  choices(name: 'namespace',
//          choices: 'dev\nqa',
//          description: 'What door do you choose?')
//  text(defaultValue: '''journeysearch:
//          image: latest
//          service2:
//                  image: latest
//          service3:
//                  image: latest''',
//          name: 'services' )
//}

// currentBuild.description = "Deployment: ${namespace}"

node("node")
        {

          def appRepo = '${GitUrl}'
          def bitbucketCredsId = 'bitbutket-key'
          def ecrRepo = '878033925423.dkr.ecr.eu-west-3.amazonaws.com'
          def ecrCredsId = 'ecr:eu-west-3:acex_test'

          cleanWs()
          try {


            def workspace = pwd()
            stage('Git checkout') {
              checkout([$class: 'GitSCM',
                        branches: [[name: '${GitBranch}']],
                        doGenerateSubmoduleConfigurations: false,
                        extensions: [
                                [$class: 'CleanCheckout']
                        ],
                        submoduleCfg: [],
                        userRemoteConfigs: [[credentialsId: bitbucketCredsId, url: appRepo]]
              ])
              shortCommit = sh(script: "git rev-parse --short HEAD", returnStdout: true).trim()
              repoName = sh(script: "git remote show origin -n |grep Fetch | sed  's/^[^/]*\\//\\//' | cut -c 2- |sed 's/.\\{4\\}\$//'", returnStdout: true).trim()

              stage('test') {
                  container('node') {
                      println 'Code has been cloned current brach is: '
                   //   sh("pwd && ls -la")
                      sh("node -v")
                      println "${namespace}"
                  }
              }
            }

            stage('Build') {
              configFileProvider([configFile(fileId: 'aws', variable: 'aws')]) {
                sh("mkdir -p ~/.aws && cp ${aws} ~/.aws/credentials")
              }
              container('node') {
                sh("echo Git Url has to be ${GitUrl}, git branch is ${GitBranch}, commit sha ${GitCommit} and ShortCommit ${shortCommit}")
                sh("rm -rf endpoints.config.json && cp -rp env/${namespace}.json endpoints.config.json")
                sh("pwd && ls -la")
                sh("cat endpoints.config.json")
                sh("npm install")
                sh("npm run app:build")

              }

            }


              stage('Deploy') {
                  configFileProvider([configFile(fileId: 'aws', variable: 'aws')]) {
                      sh("mkdir -p ~/.aws && cp ${aws} ~/.aws/credentials")
                  }
                  container('node') {
                      def bucket_name = bucketName(namespace)
                      def distribution_id = kubeContext(namespace)
                    //  distribution_id = "E3SKN13Q794MQX"
                      sh("echo Git Url has to be ${GitUrl}, git branch is ${GitBranch}, commit sha ${GitCommit} and ShortCommit ${shortCommit}")
                      sh("pwd")
                      sh("aws s3 sync --delete --acl public-read './dist/app' 's3://${bucket_name}'")
                      sh("aws cloudfront create-invalidation --distribution-id ${distribution_id} --paths '/*'")
                      //sh("tail -f /dev/null")

                  }

              }





//    sendReportToSlack(namespace, params.services)
            currentBuild.result = "SUCCESS"

          } catch(e) {
//    sendReportToSlack(namespace, params.services, e)
            currentBuild.result = "FAILED"
            println "error: " + e
            throw e
          }
        }

@NonCPS
def kubeContext(def envName) {
    switch(envName) {
        case 'dev':
            return 'E3SKN13Q794MQX'
            break
        case 'qa':
            return 'E1R8HJQJF408AI'
            break
        case 'testings':
            println "${context}"
            return '${context}'
            break
        default:
            throw new Exception ("Environment ${envName} not found")
            break
    }
}

@NonCPS
def bucketName(def envName) {
    switch(envName) {
        case 'dev':
            return 'acex-dev-test-website'
            break
        case 'qa':
            return 'acex-qa-test-website'
            break
        case 'testings':
            println "${context}"
            return '${context}'
            break
        default:
            throw new Exception ("Environment ${envName} not found")
            break
    }
}