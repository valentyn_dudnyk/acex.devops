//def appRepo = '${GitUrl}'
def appRepo = '${GitUrl}'
def TriggerNumber='${TriggerBuildNumber}'
def appBranch = '${GitBranch}'
def bitbucketCredsId = 'bitbutket-key'
def ecrRepo = '878033925423.dkr.ecr.eu-west-3.amazonaws.com'
def ecrCredsId = 'ecr:eu-west-3:acex_test'

node('buildnet') {
    cleanWs()
    checkout([$class: 'GitSCM',
              branches: [[name: '${GitBranch}']],
              doGenerateSubmoduleConfigurations: false,
              extensions: [
                      [$class: 'CleanCheckout']
              ],
              submoduleCfg: [],
              userRemoteConfigs: [[credentialsId: bitbucketCredsId, url: appRepo]]
    ])
    properties([
            parameters([
                    string(name: 'GitUrl', defaultValue: 'None', description: '', ),
                    string(name: 'GitBranch', defaultValue: 'None', description: '', ),
                    string(name: 'GitCommit', defaultValue: 'None', description: '', ),
                    string(name: 'TriggerBuildNumber', defaultValue: 'None', description: '', )
            ])
    ])

    shortCommit = sh(script: "git rev-parse --short HEAD", returnStdout: true).trim()
    repoName = sh(script: "git remote show origin -n |grep Fetch | sed  's/^[^/]*\\//\\//' | cut -c 2- |sed 's/.\\{4\\}\$//'", returnStdout: true).trim()
    reposName = sh(script: "git remote show origin -n |grep Fetch | sed  's/^[^/]*\\//\\//' | cut -c 2- |sed 's/.\\{4\\}\$//'", returnStdout: true).trim()

    container('buildnet') {
        sh("echo Git Url has to be ${GitUrl}, git branch is ${GitBranch}, commit sha ${GitCommit}")
        sh("pwd")
        sh("ls -la")
        sh("mkdir /dist")
        sh(script: "git branch", returnStdout: true)
        sh(script: "set +x; git remote show origin -n |grep Fetch | sed  's/^[^/]*\\//\\//' | cut -c 2- |sed 's/.\\{4\\}\$//'" , returnStdout: false)
        sh("echo 35.181.75.104 nexus.test.acex.tech >> /etc/hosts")
        sh("echo TriggerBuildNubmer is ${TriggerNumber}")
        def projNames = namePolicy(reposName)
        projNames.each {
            sh("cd ./src && ls")
            sh("mkdir /dist/${it}")
            sh("dotnet pack ./src/${it} -c Release /p:Version=0.1.${TriggerNumber} -o /dist/${it}")
            sh("dotnet nuget push /dist/${it}/*.nupkg  -k dbd670e9-61a5-31ea-8048-edb1866cc9b1 -s http://nexus.test.acex.tech/repository/acex-nugets-feed/")
        }
        //sh("tail -f /dev/null")
    }
}

@NonCPS
def namePolicy(def envName) {
    switch(envName) {
        case 'acex.integration.journeysearch':
            return ['Acex.Integration.JourneySearch']
            break
        case 'acex.common':
            return ['Acex.Common']
            break
        case 'acex.integration.payment':
            return ['Acex.Integration.Payment.Contracts']
            break
        case 'acex.integration.hafas':
            return ['Acex.Integration.Hafas']
            break
        case 'acex.integration.timetable':
            return ['Acex.Integration.Timetable']
            break
        case 'acex.integration.location':
            return ['Acex.Integration.Location']
            break
        case 'acex.integration.userprofile':
            return ['Acex.Integration.UserProfile']
            break
        case 'acex.integration.fares':
            return ['Acex.Integration.Fares']
            break
        case 'acex.rest.client':
            return ['Acex.RestClient', 'Acex.RestClient.Interfaces']
            break
        case 'acex.api.infra':
            return ['Acex.AspNet']
            break
        case 'acex.http':
            return ['Acex.ServiceClient', 'Acex.Http.Client', 'Acex.Http.Host', 'Acex.Http']
            break
        case 'acex.timetable':
            return ['Acex.Timetable.Contracts', 'Acex.Timetable.Client.Rest', 'Acex.Integration.Timetable.Contracts']
            break
        case 'acex.notification':
            return ['Acex.Email.Contracts', 'Acex.Email.Client.Rest']
            break
        case 'acex.journeysearch':
            return ['Acex.JourneySearch.Contracts', 'Acex.JourneySearch.Client.Rest', 'Acex.Integration.JourneySearch.Contracts']
            break
        case 'acex.location':
            return ['Acex.Location.Contracts', 'Acex.Location.Client.Rest', 'Acex.Integration.Location.Contracts']
            break
        case 'acex.identityserver':
            return ['Acex.IdentityServer.Client.Rest', 'Acex.IdentityServer.Contracts']
            break
        case 'acex.email':
            return ['Acex.Email.Client.Rest', 'Acex.Email.Contracts']
            break
        default:
            throw new Exception ("Environment ${envName} not found")
            break
    }
}